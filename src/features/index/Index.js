import React from 'react'
import {Grid} from 'react-bootstrap'
import ComponentRow from '../../components/index/ComponentRow'

class Index extends React.Component {
    constructor(props, state) {
        super(props, state)
    }

    render() {
        return (
            <Grid>
                <ComponentRow/>
                <ComponentRow/>
            </Grid>
        )
    }
}

export default Index