import React from 'react'
import ReactDOM from 'react-dom'
import {createStore} from 'redux'

import {Router} from 'react-router-dom'
import Index from './features/index'


const store = createStore()

ReactDOM.render(
    <Router>
        <Index store={store}/>
    </Router>
    , document.getElementById('phss-root')
)
