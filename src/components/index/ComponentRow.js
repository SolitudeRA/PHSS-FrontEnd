import React from 'react'
import {Row} from 'react-bootstrap'
import ComponentPart from './ComponentPart'

class ComponentRow extends React.Component {
    render() {
        return (
            <Row>
                <ComponentPart/>
                <ComponentPart/>
                <ComponentPart/>
                <ComponentPart/>
            </Row>
        )
    }
}

export default ComponentRow